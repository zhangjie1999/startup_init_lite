# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//base/startup/init_lite/begetd.gni")

param_include_dirs = [
  "//base/startup/init_lite/services/param/include",
  "//base/startup/init_lite/services/param/adapter",
  "//base/startup/init_lite/services/include/param",
  "//base/startup/init_lite/services/include",
  "//base/startup/init_lite/services/init/include",
  "//base/startup/init_lite/services/log",
  "//base/startup/init_lite/services/loopevent/include",
  "//third_party/bounds_checking_function/include",
  "//third_party/cJSON",
  "//utils/native/lite/include",
]

param_build_defines = [
  "_GNU_SOURCE",
  "INCREMENTAL_VERSION=\"${ohos_version}\"",
  "BUILD_TYPE=\"${ohos_build_type}\"",
  "BUILD_USER=\"${ohos_build_user}\"",
  "BUILD_TIME=\"${ohos_build_time}\"",
  "BUILD_HOST=\"${ohos_build_host}\"",
  "BUILD_ROOTHASH=\"${ohos_build_roothash}\"",
]

if (enable_ohos_startup_init_feature_begetctl_liteos) {
  action("lite_const_param_to") {
    script = "//base/startup/init_lite/scripts/param_cfg_to_code.py"
    args = [
      "--source",
      rebase_path(
          "//base/startup/init_lite/services/etc_lite/param/ohos_const/ohos.para"),
      "--dest_dir",
      rebase_path("$root_out_dir/gen/init_lite/"),
      "--priority",
      "0",
    ]
    outputs = [ "$target_gen_dir/${target_name}_param_cfg_to_code.log" ]
  }

  action("lite_ohos_param_to") {
    script = "//base/startup/init_lite/scripts/param_cfg_to_code.py"
    args = [
      "--source",
      rebase_path("//base/startup/init_lite/services/etc/param/ohos.para"),
      "--dest_dir",
      rebase_path("$root_out_dir/gen/init_lite/"),
      "--priority",
      "0",
    ]
    outputs = [ "$target_gen_dir/${target_name}_param_cfg_to_code.log" ]
  }

  action("vendor_param_to") {
    script = "//base/startup/init_lite/scripts/param_cfg_to_code.py"
    args = [
      "--source",
      rebase_path("$ohos_product_adapter_dir/utils/sys_param/vendor.para"),
      "--dest_dir",
      rebase_path("$root_out_dir/gen/init_lite/"),
      "--priority",
      "1",
    ]
    outputs = [ "$target_gen_dir/${target_name}_param_cfg_to_code.log" ]
  }
}

config("exported_header_files") {
  visibility = [ ":*" ]
  include_dirs = [
    "//base/startup/init_lite/interfaces/innerkits/include",
    "//base/startup/init_lite/interfaces/service/include/param",
    "//base/startup/init_lite/interfaces/service/param/include",
  ]
}

base_sources = [
  "//base/startup/init_lite/services/log/init_commlog.c",
  "//base/startup/init_lite/services/param/base/param_base.c",
  "//base/startup/init_lite/services/param/base/param_comm.c",
  "//base/startup/init_lite/services/param/base/param_trie.c",
  "//base/startup/init_lite/services/param/liteos/param_client.c",
  "//base/startup/init_lite/services/param/liteos/param_litedac.c",
  "//base/startup/init_lite/services/param/liteos/param_osadp.c",
  "//base/startup/init_lite/services/param/manager/param_manager.c",
  "//base/startup/init_lite/services/param/manager/param_persist.c",
  "//base/startup/init_lite/services/utils/init_hashmap.c",
  "//base/startup/init_lite/services/utils/list.c",
]

static_library("param_init_lite") {
  defines = []
  deps = []
  sources = [
    "//base/startup/init_lite/services/param/manager/param_manager.c",
    "//base/startup/init_lite/services/param/manager/param_server.c",
  ]
  include_dirs = param_include_dirs
  defines += param_build_defines
  public_configs = [ ":exported_header_files" ]

  if (ohos_kernel_type == "liteos_a") {
    sources += [
      "//base/startup/init_lite/services/param/adapter/param_persistadp.c",
      "//base/startup/init_lite/services/param/liteos/param_service.c",
      "//base/startup/init_lite/services/param/manager/param_persist.c",
    ]
    defines += [
      "WORKSPACE_AREA_NEED_MUTEX",
      "PARAM_PERSIST_SAVE_MUTEX",
      "PARAMWORKSPACE_NEED_MUTEX",
      "__LITEOS_A__",
      "PARAM_SUPPORT_CYCLE_CHECK",
    ]
  }
}

static_library("param_client_lite") {
  sources = base_sources
  include_dirs = param_include_dirs
  cflags = [ "-fPIC" ]
  defines = param_build_defines
  public_configs = [ ":exported_header_files" ]

  if (ohos_kernel_type == "liteos_a") {
    sources +=
        [ "//base/startup/init_lite/services/param/adapter/param_persistadp.c" ]
    defines += [
      "__LITEOS_A__",
      "WORKSPACE_AREA_NEED_MUTEX",
      "PARAM_PERSIST_SAVE_MUTEX",
      "PARAMWORKSPACE_NEED_MUTEX",
    ]
  } else if (ohos_kernel_type == "liteos_m") {
    sources +=
        [ "//base/startup/init_lite/services/param/manager/param_server.c" ]
    if (enable_ohos_startup_init_lite_use_posix_file_api) {
      sources += [
        "//base/startup/init_lite/services/param/adapter/param_persistadp.c",
      ]
    } else {
      sources +=
          [ "//base/startup/init_lite/services/param/liteos/param_hal.c" ]
    }
    defines += [
      "__LITEOS_M__",
      "WORKSPACE_AREA_NEED_MUTEX",
      "PARAM_PERSIST_SAVE_MUTEX",
      "PARAMWORKSPACE_NEED_MUTEX",
      "DATA_PATH=\"${config_ohos_startup_init_lite_data_path}\"",
    ]
    if (enable_ohos_startup_init_feature_begetctl_liteos) {
      deps = [
        ":lite_const_param_to",
        ":lite_ohos_param_to",
        ":vendor_param_to",
      ]
      include_dirs += [ "$root_out_dir/gen/init_lite" ]
      defines += [ "PARAM_LOAD_CFG_FROM_CODE" ]
    }
  }
}
